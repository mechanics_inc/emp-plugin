var ENGINEERS_CAPACITY = 120;
var zoom = 0;
var updRate = 300;
var plaginOn = true;
var settingsOn = false;
var mode = 'Resources';

BuildInterface();

var updRateVal = document.getElementById('updRateVal');
var plagSettings = document.getElementById('plagSettings');

var timerId = setTimeout(
    function findAnomalies() {
	    if(plaginOn){
		    var value = element.value;

		    SetZoom();
			
		    var elems = document.getElementsByTagName('a');
		    for (var i = 0; i < elems.length; i++) {
			    if (elems[i].className.indexOf('z' + zoom) > -1) {
					var elemTitle = elems[i].title;

				    if (elems[i].style.border == '3px')
					    return;
				    else if (
				    	elemTitle.indexOf('Threat level: ' + value) > -1 ||
					    elemTitle.indexOf('Уровень опасности: ' + value) > -1 ||
					    elemTitle.indexOf('Рівень небезпеки: ' + value) > -1
					    ) {
					    elems[i].style.border = '3px solid red';
					    elems[i].style.opacity = '100';
				    } 
				    else if (
				    	elemTitle.indexOf('Comet<br/>Level: ' + value) > 1 ||
					    elemTitle.indexOf('Комета<br/>Уровень: ' + value) > 1
					    ) {
					    elems[i].style.border = '3px solid green';
					    elems[i].style.opacity = '100';
				    }
				    else if (
				    	mode == 'Planets' &&
				    	elems[i].innerHTML == '' &&
				    	(
					    	(elemTitle.indexOf('Звездная система') > 1) ||
					    	(elemTitle.indexOf('Зоряна система') > 1) ||
					    	(elemTitle.indexOf('Star system') > 1)
				    	)){
				        var left = elems[i].style.left;
				        var top = elems[i].style.top;
				        var ptl = CalculatePlacesToLive(elemTitle);
				        elems[i].innerHTML = '<div style="color:' + CalculateColor(ptl) + ';position:relative;top:-10px;left:-10px;font-weight:bold;">' + ptl + '</div>';
				        elems[i].style.opacity = '1';
				    }
				    else if (mode == 'Resources') {
					    elems[i].style.border = '0';
					    elems[i].style.opacity = '0';
				    }
			    }
				
			    var type = GetResourseType(elems[i]);
			    switch (type) {
			    case 'iron':
				    elems[i].style.borderTop = '6px solid #EEDC82';
				    break;
			    case 'polymer':
				    elems[i].style.borderTop = '6px solid #6495ED';
				    break;
			    case 'radium':
				    elems[i].style.borderTop = '6px solid #34C924';
				    break;
			    }
		    }
        }
        setTimeout(findAnomalies, updRate);
    }, updRate);

function CalculatePlacesToLive(elementTitle) {
    var small_iron = 6;
    var medium_iron = 10;
    var small_polymer = 6;
    var medium_polymer = 10;
    var small_radium = 6;
    var medium_radium = 10;
    var mars = 10;
    var reach_mars = 12;
    var earth = 16;
    var super_earth = 22;

    var txt_small_iron = 'small-iron';
    var txt_medium_iron = 'medium-iron';
    var txt_small_polymer = 'small-polymer';
    var txt_medium_polymer = 'medium-polymer';
    var txt_small_radium = 'small-radium';
    var txt_medium_radium = 'medium-radium';
    var txt_mars = 'span class="mars ';
    var txt_reach_mars = 'reach-mars';
    var txt_earth = 'span class="earth ';
    var txt_super_earth = 'super-earth';

    var entries_count_small_iron = GetNumberOfEntries(elementTitle, txt_small_iron);
    var entries_count_medium_iron = GetNumberOfEntries(elementTitle, txt_medium_iron);
    var entries_count_small_polymer = GetNumberOfEntries(elementTitle, txt_small_polymer);
    var entries_count_medium_polymer = GetNumberOfEntries(elementTitle, txt_medium_polymer);
    var entries_count_small_radium = GetNumberOfEntries(elementTitle, txt_small_radium);
    var entries_count_medium_radium = GetNumberOfEntries(elementTitle, txt_medium_radium);
    var entries_count_mars = GetNumberOfEntries(elementTitle, txt_mars);
    var entries_count_reach_mars = GetNumberOfEntries(elementTitle, txt_reach_mars);
    var entries_count_earth = GetNumberOfEntries(elementTitle, txt_earth);
    var entries_count_super_earth = GetNumberOfEntries(elementTitle, txt_super_earth);

    return placesToLive =
        (small_iron * entries_count_small_iron) +
        (medium_iron * entries_count_medium_iron) +
        (small_polymer * entries_count_small_polymer) +
        (medium_polymer * entries_count_medium_polymer) +
        (small_radium * entries_count_small_radium) +
        (medium_radium * entries_count_medium_radium) +
        (mars * entries_count_mars) +
        (reach_mars * entries_count_reach_mars) +
        (earth * entries_count_earth) +
        (super_earth * entries_count_super_earth);
}

function CalculateColor(placesToLive){

	var color = '';

	if(placesToLive >= 60)
		color = '#00ff16';
	else if(placesToLive >= 55)
		color = 'yellow';
	else if(placesToLive >= 50)
		color = '#a9a900';
	else if(placesToLive >=40)
		color = '#808080';
	else if(placesToLive >=30)
		color = '#5a5a5a';
	else if(placesToLive >=20)
		color = '#252525';
	else
		color = 'black'

	return color;
}

function GetNumberOfEntries(str, symbols) {
    return count = (str.split(symbols).length - 1);
}

function BuildInterface(){
	var area = document.getElementById('game-frame');

	var plaginArea = document.createElement('div');
	var plaginInt = '<style type="text/css">' + 
						'#plaginArea{' + 
							'z-index:999;' + 
							'position:fixed;' + 
							'left:0;' + 
						'}' + 
						'.plagContent{' + 
							'float:left;' + 
							'width:4.5rem;' + 
						'}' + 
						'#plagSettings{' + 
							'float:left;' + 
							'text-align:center;' +
							'background:#040E17;' + 
                            'display:none;' + 
						'}' + 
						'.inpts{' + 
							'float:left;' + 
							'width:3rem;' + 
							'height:3rem !important;' + 
							'text-align:center;' + 
							'font-size:1rem;' + 
							'font-weight:bold;' + 
							'background:#040E17;' + 
						'}' + 
						'.btnsArea{' + 
							'float:left;' + 
							'width:1.5rem;' + 
						'}' + 
						'.btns{' + 
							'float:left;' + 
							'width:1.5rem;' + 
							'height:1.5rem;' + 
							'font-size:1rem;' + 
							'padding:0;' + 
							'font-weight:bold;' + 
							'background:#040E17;' + 
							'color:#FFFFFF;' + 
							'border:0;' + 
						'}' + 
					'</style>' + 
					
					'<div class="plagContent">' + 
						'<div class="plagHeader">' + 
							'<button id="plaginOffBtn" class="btns" style="width:3rem;">OFF</button>' + 
							'<button id="emp-plug-but-settings" class="btns" >&#9874;</button>' + 
						'</div>' + 
						'<input class="inpts" type="number" id="element" value="4" >' + 
						'<div class="btnsArea">' + 
							'<button id="emp-plug-but-up" class="btns" >&#9650;</button>' + 
							'<button id="emp-plug-but-down" class="btns" >&#9660;</button>' + 
						'</div>' + 
					'</div>' + 
					'<div id="plagSettings">' +
						'<p style="margin:0;"><div id="updRateVal">Update rate (' + updRate / 1000 + ' sec)</div></p>' +
						'<input id="emp-plug-ev-change" style="width:15rem;" type="range" min="100" max="1000" step="10" value="' + updRate + '">' + 
							'<table style="width:100%;text-align:left;">' +
								'<tr>' +
									'<td>'+
										'<input style="width:100%;text-align:left;" type="number" id="enginCap" value="120" >' + 
									'</td>'+
									'<td>'+
										' Engineers capacity' +
									'</td>'+
								'</tr>' +
								'<tr>' +
									'<td>'+
										'<select id="checkMode" style="width:100%;text-align:left;">' +
											'<option selected value="Resources">Resources</option>' +
											'<option value="Planets">Planets</option>' +
										'</select>' +
									'</td>'+
									'<td>'+
										' Mode' +
									'</td>'+
								'</tr>' +
							'</table>' +
					'</div>';

	plaginArea.innerHTML = plaginInt;
	plaginArea.id = 'plaginArea';
						
	area.appendChild(plaginArea);

	elem = document.getElementById('plaginOffBtn');
	elem.addEventListener("click", OnOffPlagin, false);

	elem = document.getElementById('emp-plug-but-settings');
	elem.addEventListener("click", OnOffSettings, false);

	elem = document.getElementById('emp-plug-but-up');
	elem.addEventListener("click", upVal, false);

	elem = document.getElementById('emp-plug-but-down');
	elem.addEventListener("click", downVal, false);

	elem = document.getElementById('element');
	elem.addEventListener("change", CheckInputValue, false);

	elem = document.getElementById('emp-plug-ev-change');
	elem.addEventListener("change", ChangeUpdRate, false);
	
	elem = document.getElementById('enginCap');
	elem.addEventListener("change", GetEnginCap, false);
	
	elem = document.getElementById('checkMode');
	elem.addEventListener("change", CheckMode, false);
}

function CheckMode(){
	mode = document.getElementById('checkMode').value;
}

function ChangeUpdRate() {
	value = elem = document.getElementById('emp-plug-ev-change').value;
	
    updRate = parseInt(value);
    updRateVal.innerHTML = 'Update rate (' + value / 1000 + ' sec)';
}

function OnOffPlagin(){
	elem = document.getElementById('plaginOffBtn');

	if(plaginOn){
	    elem.innerHTML = 'ON';
	}
	else {
	    elem.innerHTML = 'OFF';
	}
		
	plaginOn = !plaginOn;
}

function OnOffSettings(){
    if (settingsOn) {
        plagSettings.style.display = 'none';
	}
    else {
        plagSettings.style.display = 'inline';
	}

    settingsOn = !settingsOn;
}

function SetZoom() {
    var zoomElem = document.getElementsByClassName('znav-block');
    for (var i = 0; i < zoomElem.length; i++) {
        zoom = zoomElem[i].innerHTML.substring(0, 1);
    }
}

function GetResourseType(element) {
    if (element.title == '')
        return '';
    
    div = document.createElement('div'),
    div.innerHTML = element.title;
    
    var resoursType = '';
    var iron = 0;
    var polymer = 0;
    var radium = 0;
    var spans = div.getElementsByTagName('span');
    
    for (var i = 0; i < spans.length; i++) {
        if (spans[i].className.indexOf('iron') > 1) {
            iron = parseInt(spans[i].innerHTML);
        } 
        else if (spans[i].className.indexOf('polymer') > 1) {
            polymer = parseInt(spans[i].innerHTML);
        } 
        else if (spans[i].className.indexOf('radium') > 1) {
            radium = parseInt(spans[i].innerHTML);
        }
    }
    
    if (iron == 0 && polymer == 0 && radium == 0)
        return '';
    var resourses = [iron, polymer, radium];
    var maxVal = GetMaxValue(resourses);
    
    if (maxVal == iron)
        resoursType = 'iron';
    else if (maxVal == polymer)
        resoursType = 'polymer';
    else if (maxVal == radium)
        resoursType = 'radium';
    
    if ((element.title.indexOf('Comet') > 1 || element.title.indexOf('Комета') > 1) && element.title.indexOf('Нужно инженеров') == (-1))
        element.title = element.title + '<br/><div class="enginCountAreas">Нужно инженеров: ' + CalculateEngineersCount(resourses) + '</div>';
    
    return resoursType;
}

function CheckInputValue(){
	elem = document.getElementById('element');

	var val = parseInt(elem.value);
	if(val > 20){
		elem.value = 20;
	}else if (val < 0 || isNaN(val)){
		elem.value = 0;
	}
}

function GetEnginCap(){
	var enginCap = document.getElementById('enginCap');
	ENGINEERS_CAPACITY = parseInt(enginCap.value);
}

function CalculateEngineersCount(array) {
    var resourcesSumm = array[0] + array[1] + array[2];
    var engineersCount = Math.floor(resourcesSumm / ENGINEERS_CAPACITY);
    return engineersCount;
}

function upVal() {
    element.value = parseInt(element.value) + 1;
}

function downVal() {
    element.value = parseInt(element.value) - 1;
}

function GetMaxValue(array) {
    return Math.max.apply(Math, array);
}

function GetMinValue(array) {
    return Math.min.apply(Math, array);
}
