var gulp = require('gulp');
var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');
var jsonminify = require('gulp-jsonminify');

gulp.task('default', function() {
    gulp.src('app/**/*.js')
	.pipe(jsmin())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('dist'));
	
    gulp.src(['app/*.json'])
	.pipe(jsonminify())
	.pipe(gulp.dest('dist'));
});